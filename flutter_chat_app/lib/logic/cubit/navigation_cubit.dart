// Dart imports:
import 'dart:async';

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// Project imports:
import 'package:flutter_chat_app/presentation/page/home_page.dart';

part 'navigation_state.dart';

class NavigationCubit extends Cubit<NavigationState> {
  NavigationCubit({
    List<Widget> pages = const [
      HomePage(key: ValueKey("HomePage")),
    ],
  }) : super(NavigationState(pages));

  Future<void> pop([dynamic result]) async {
    final newState = state.pop(result);

    emit(newState);
  }

  Future<void> popUntilRoot() async {
    final newState = state.popUntilRoot();

    emit(newState);
  }

  bool onPopPage(Route<dynamic> route, dynamic result) {
    final newState = state.onPopPage(route, result);

    if (state == newState) return false;

    emit(newState);
    return true;
  }

  void navigateToHomePage() {
    if (state.currentPage is HomePage) return;

    final newState = state.popUntilEmpty().push(
          const HomePage(
            key: ValueKey("HomePage"),
          ),
        );

    emit(newState);
  }
}
