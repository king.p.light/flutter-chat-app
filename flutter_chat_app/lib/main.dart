// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:easy_localization/easy_localization.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

// Project imports:
import 'package:flutter_chat_app/constant/theme_constant.dart';
import 'package:flutter_chat_app/logic/cubit/navigation_cubit.dart';
import 'package:flutter_chat_app/presentation/widget/scrollbar_behavior.dart';
import 'package:flutter_chat_app/util/locator.dart';

Future<void> main() async {
  // This will turn Object.toString() to readable format
  EquatableConfig.stringify = true;

  // Initialize WidgetsFlutterBinding
  WidgetsFlutterBinding.ensureInitialized();

  // Initialize EasyLocalization
  EasyLocalization.logger.enableLevels = [];
  await EasyLocalization.ensureInitialized();

  // Setup dependency injection
  setupLocator();

  runApp(const App());
}

class App extends StatefulWidget {
  const App({Key? key}) : super(key: key);

  @override
  _AppState createState() => _AppState();
}

class _AppState extends State<App> {
  // late final _authCubit = AuthCubit(navigationCubit: _navigationCubit);
  late final _navigationCubit = NavigationCubit();

  @override
  void dispose() {
    // _authCubit.close();
    _navigationCubit.close();

    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return EasyLocalization(
      supportedLocales: const [
        Locale("en"),
        Locale("th"),
      ],
      path: "assets/translations",
      fallbackLocale: const Locale("en"),
      child: MultiBlocProvider(
        providers: [
          // BlocProvider<AuthCubit>.value(value: _authCubit),
          BlocProvider<NavigationCubit>.value(value: _navigationCubit),
        ],
        child: Builder(
          builder: (context) {
            return GestureDetector(
              onTap: () {
                WidgetsBinding.instance.focusManager.primaryFocus?.unfocus();
              },
              child: MaterialApp(
                title: "IoT Gateway Publisher Demo",
                theme: ThemeConstant.theme,
                localizationsDelegates: context.localizationDelegates,
                supportedLocales: context.supportedLocales,
                locale: context.deviceLocale,
                scrollBehavior: ApplicationScrollbarBehavior(),
                debugShowCheckedModeBanner: false,
                home: BlocBuilder<NavigationCubit, NavigationState>(
                  builder: (context, state) {
                    return Navigator(
                      pages: state.pages.map((page) {
                        return MaterialPage(
                          key: ValueKey(page.runtimeType),
                          child: _withBlocProviders(page),
                        );
                      }).toList(),
                      onPopPage: context.read<NavigationCubit>().onPopPage,
                    );
                  },
                ),
              ),
            );
          },
        ),
      ),
    );
  }

  Widget _withBlocProviders(Widget child) {
    final List<BlocProvider> providers = [
      BlocProvider<NavigationCubit>.value(value: _navigationCubit),
    ];

    if (providers.isEmpty) return child;

    return MultiBlocProvider(
      providers: providers,
      child: child,
    );
  }
}
