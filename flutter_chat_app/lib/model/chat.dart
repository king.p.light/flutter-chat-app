// Package imports:

import 'package:equatable/equatable.dart';
import 'package:flutter_chat_app/data/api/application_api/response/chat_response.dart';

// Project imports:

import 'package:json_annotation/json_annotation.dart';

// part 'chat.g.dart';

@JsonSerializable()
class Chat extends Equatable {
  final String name;
  final String lastMessage;
  final String image;
  final String time;
  final bool isActive;

  const Chat({
    required this.name,
    required this.lastMessage,
    required this.image,
    required this.time,
    required this.isActive,
  });

  factory Chat.fromChatResponse(ChatResponse response) {
    return Chat(
      name: response.name,
      lastMessage: response.lastMessage,
      image: response.image,
      time: response.time,
      isActive: response.isActive,
    );
  }

  @override
  List<Object?> get props => [
        name,
        lastMessage,
        image,
        time,
        isActive,
      ];
}
