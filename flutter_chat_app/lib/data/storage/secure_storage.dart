// Package imports:
import 'package:flutter_secure_storage/flutter_secure_storage.dart';

// Project imports:
import 'package:flutter_chat_app/constant/enum/secure_storage_key_enum.dart';
import 'package:flutter_chat_app/util/locator.dart';

class SecureStorage {
  final secureStorage = locator.get<FlutterSecureStorage>();

  Future<void> deleteAll() async {
    await secureStorage.deleteAll();
  }

  Future<String?> getUsername() async {
    return await secureStorage.read(
      key: SecureStorageKeyEnum.username.name,
    );
  }

  Future<void> setUsername(String value) async {
    await secureStorage.write(
      key: SecureStorageKeyEnum.username.name,
      value: value,
    );
  }

  Future<String?> getAccessToken() async {
    return await secureStorage.read(
      key: SecureStorageKeyEnum.accessToken.name,
    );
  }

  Future<void> setAccessToken(String value) async {
    await secureStorage.write(
      key: SecureStorageKeyEnum.accessToken.name,
      value: value,
    );
  }

  Future<String?> getRefreshToken() async {
    return await secureStorage.read(
      key: SecureStorageKeyEnum.refreshToken.name,
    );
  }

  Future<void> setRefreshToken(String value) async {
    await secureStorage.write(
      key: SecureStorageKeyEnum.refreshToken.name,
      value: value,
    );
  }
}
