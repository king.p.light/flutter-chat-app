// Package imports:
import 'package:shared_preferences/shared_preferences.dart';

// Project imports:
import 'package:flutter_chat_app/constant/enum/shared_prefs_key_enum.dart';
import 'package:flutter_chat_app/util/locator.dart';

class SharedPrefs {
  final _sharedPreferences = locator.getAsync<SharedPreferences>();

  Future<void> deleteAll() async {
    final sharedPreferences = await _sharedPreferences;

    await sharedPreferences.clear();
  }

  Future<DateTime?> getAccessTokenExpirationDate() async {
    final sharedPreferences = await _sharedPreferences;

    final stringValue = sharedPreferences
        .getString(SharedPrefsKeyEnum.accessTokenExpirationDate.name);

    if (stringValue == null) return null;

    return DateTime.parse(stringValue);
  }

  Future<void> setAccessTokenExpirationDate(DateTime value) async {
    final sharedPreferences = await _sharedPreferences;

    await sharedPreferences.setString(
      SharedPrefsKeyEnum.accessTokenExpirationDate.name,
      value.toIso8601String(),
    );
  }

  Future<DateTime?> getNotificationReadDate() async {
    final sharedPreferences = await _sharedPreferences;

    final stringValue = sharedPreferences
        .getString(SharedPrefsKeyEnum.notificationReadDate.name);

    if (stringValue == null) return null;

    return DateTime.parse(stringValue);
  }

  Future<void> setNotificationReadDate(DateTime value) async {
    final sharedPreferences = await _sharedPreferences;

    await sharedPreferences.setString(
      SharedPrefsKeyEnum.notificationReadDate.name,
      value.toIso8601String(),
    );
  }
}
