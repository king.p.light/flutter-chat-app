/* This is an example

// Package imports:
import 'package:hive/hive.dart';

// Project imports:
import 'package:application_template/data/api/response/git_hub_user_response.dart';

part 'user_entity.g.dart';

@HiveType(typeId: 0)
class UserEntity extends HiveObject {
  @HiveField(0)
  late int id;

  @HiveField(1)
  late String name;

  @HiveField(2)
  late String avatarUrl;

  static UserEntity fromResponse(GitHubUserResponse response) {
    return UserEntity()
      ..id = response.id
      ..name = response.login
      ..avatarUrl = response.avatarUrl;
  }
}
*/
