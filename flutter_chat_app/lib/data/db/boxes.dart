/* This is an example

// Package imports:
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

// Project imports:
import 'package:application_template/data/storage/entity/user_entity.dart';

class Boxes {
  static Box<UserEntity> getUsers() {
    return Hive.box<UserEntity>("users");
  }

  static Future<void> initialize() async {
    await Hive.initFlutter();

    Hive.registerAdapter(UserEntityAdapter());

    await Hive.openBox<UserEntity>("users");
  }
}
*/
