// Dart imports:

// Flutter imports:

// Package imports:
import 'package:dio/dio.dart';
import 'package:dio/native_imp.dart';

class MockAPIEndpoint extends DioForNative {
  MockAPIEndpoint() : super() {
    interceptors.add(
      InterceptorsWrapper(
        onRequest: (options, handler) async {
          await Future.delayed(const Duration(milliseconds: 500));

          handler.resolve(
            Response<dynamic>(
              data: options.method == "GET"
                  ? _createGetResponse(options)
                  : _createPostResponse(options),
              requestOptions: options,
            ),
          );
        },
      ),
    );
  }

  Map<String, dynamic> _createGetResponse(RequestOptions options) {
    switch (options.path) {
      // case "/notifications":
      //   if (options.headers["accessToken"] == "Bearer abc123")
      //     return {
      //       "data": [
      //         {
      //           "id": 0,
      //           "type": "actionRequired",
      //           "title": "Something important",
      //           "message": "Please review",
      //         },
      //       ]
      //     };

      //   return {
      //     "error": {
      //       "code": 2,
      //       "message": "Invalid access token or expired",
      //     }
      //   };

      case "/chat":
        return {
          "person_1": [
            {
              "name": "Jenny Wilson",
              "lastMessage": "Hope you are doing well...",
              "image": "assets/images/user.png",
              "time": "3m ago",
              "isActive": false,
            }
          ],
          "person_2": [
            {
              "name": "Esther Howard",
              "lastMessage": "Hello Abdullah! I am...",
              "image": "assets/images/user_2.png",
              "time": "8m ago",
              "isActive": true,
            }
          ],
          "person_3": [
            {
              "name": "Ralph Edwards",
              "lastMessage": "Do you have update...",
              "image": "assets/images/user_3.png",
              "time": "5d ago",
              "isActive": false,
            }
          ],
          "person_4": [
            {
              "name": "Jacob Jones",
              "lastMessage": "You are welcome :)",
              "image": "assets/images/user_4.png",
              "time": "5d ago",
              "isActive": true,
            }
          ],
          "person_5": [
            {
              "name": "Albert Flores",
              "lastMessage": "Thanks",
              "image": "assets/images/user_5.png",
              "time": "6d ago",
              "isActive": false,
            }
          ],
          "person_6": [
            {
              "name": "Jenny Wilson",
              "lastMessage": "Hope you are doing well...",
              "image": "assets/images/user.png",
              "time": "3m ago",
              "isActive": false,
            }
          ],
          "person_7": [
            {
              "name": "Esther Howard",
              "lastMessage": "Hello Abdullah! I am...",
              "image": "assets/images/user_2.png",
              "time": "8m ago",
              "isActive": true,
            }
          ],
          "person_8": [
            {
              "name": "Ralph Edwards",
              "lastMessage": "Do you have update...",
              "image": "assets/images/user_3.png",
              "time": "5d ago",
              "isActive": false,
            }
          ],
        };

      default:
        throw UnimplementedError(
          "MockAppEndpoint: GET ${options.path} is not defined",
        );
    }
  }

  Map<String, dynamic> _createPostResponse(RequestOptions options) {
    switch (options.path) {
      case "/Log_In-API":
        return {"username": "test1", "token": "1234-e334d"};

      case "/Get_Reservation-API":
        return {
          "_id": "62d7b50e16c544d45adc12ad",
          "Reservation": "0004831485",
          "ReservationItem": "0001",
          "RequirementType": "MR",
          "ReservStatus": "M",
          "ItemDeleted": "",
          "MovementAllowed": "X",
          "FinalIssue": "",
          "Material": "000000000020006440",
          "Plant": "1200",
          "StorageLocation": "2001",
          "RequirementDate": "20210902",
          "RequirementQuantity": "500.000",
          "BaseUnitOfMeasure": "TAB",
          "DebitCreditInd": "H",
          "QuantityWithdrawn": "0.000",
          "QtyinUnOfEntry": "500.000",
          "UnitOfEntry": "500",
          "MovementType": "311",
          "reservation_id": "0004831485",
          "qr_system": {
            "status": "Ready for Delivery",
            "note": {
              "Ready for Delivery": "The box is big and red.",
              "Delivering": "uudbfuv",
              "Delivered": "uirvybje",
              "Waiting for Return": "uirvybje",
              "Returned": "uirvybje"
            },
            "metadata": {
              "box_size": {"0004831485_1": "S", "0004831485_2": "M"}
            }
          }
        };

      default:
        throw UnimplementedError(
          "MockAppEndpoint: POST ${options.path} is not defined",
        );
    }
  }
}
