// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'chat_response.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

ChatResponse _$ChatResponseFromJson(Map<String, dynamic> json) => ChatResponse(
      json['name'] as String,
      json['lastMessage'] as String,
      json['image'] as String,
      json['time'] as String,
      json['isActive'] as bool,
    );

Map<String, dynamic> _$ChatResponseToJson(ChatResponse instance) =>
    <String, dynamic>{
      'name': instance.name,
      'lastMessage': instance.lastMessage,
      'image': instance.image,
      'time': instance.time,
      'isActive': instance.isActive,
    };
