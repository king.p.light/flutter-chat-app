// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:flutter_chat_app/constant/enum/env.dart';
import 'package:flutter_chat_app/constant/exception/application_exception.dart';
import 'package:flutter_chat_app/constant/exception/auth_exception.dart';
import 'package:flutter_chat_app/constant/exception/unknown_exception.dart';
import 'package:flutter_chat_app/constant/exception/validation_exception.dart';
import 'package:flutter_chat_app/util/locator.dart';

part 'exception_response.g.dart';

@JsonSerializable()
class ExceptionResponse {
  final String? code;
  final String message;

  final List<Map<String, dynamic>>? details;

  const ExceptionResponse(
    this.code,
    this.message,
    this.details,
  );

  factory ExceptionResponse.fromJson(Map<String, dynamic> json) {
    return _$ExceptionResponseFromJson(json);
  }

  Exception toException() {
    switch (message) {
      case "Validation Failed":
        final details = this.details ?? [];
        return ValidationException(
          details.isNotEmpty
              ? details.first.values.map((e) => e.toString()).toList()
              : [],
        );
      case "invalid_grant (Invalid user credentials)":
        return AuthException.invalidGrant;
      case "Username already exists":
        return AuthException.usernameAlreadyTaken;
      case "Reservation is not found":
        return APIException.notFound;
    }

    switch (code) {
      case "ACCESS_DENIED":
      case "INVALID_ACCESS_TOKEN":
      case "NOT_FOUND_USER":
        return AuthException.accessDenied;
      case "NOT_FOUND_STATION":
        return APIException.stationNotFound;

      case "FILE_NOT_FOUND":
        return APIException.fileNotFound;

      default:
        final exception = UnknownException(
          content: locator.get<Env>() == Env.prod ? null : code,
        );

        // FirebaseCrashlytics.instance.recordError(
        //   exception,
        //   StackTrace.current,
        // );

        return exception;
    }
  }
}
