// Package imports:
import 'package:json_annotation/json_annotation.dart';

// Project imports:
import 'package:flutter_chat_app/constant/exception/auth_exception.dart';
import 'package:flutter_chat_app/constant/exception/unknown_exception.dart';

part 'error_response.g.dart';

@JsonSerializable()
class ErrorResponse {
  final int code;
  final String message;

  const ErrorResponse(
    this.code,
    this.message,
  );

  factory ErrorResponse.fromJson(Map<String, dynamic> json) {
    return _$ErrorResponseFromJson(json);
  }
}

extension ErrorResponseExtension on ErrorResponse {
  Exception asException() {
    switch (code) {
      case 0:
        return AuthException.invalidGrant;
      case 1:
        return AuthException.usernameAlreadyTaken;
      case 2:
        return AuthException.accessDenied;
      default:
        // ignore: avoid_print
        print("ErrorResponse: Unhandled Exception with code $code");
        return UnknownException();
    }
  }
}
