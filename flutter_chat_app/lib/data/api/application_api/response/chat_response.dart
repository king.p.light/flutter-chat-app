// Package imports:

import 'package:json_annotation/json_annotation.dart';

part 'chat_response.g.dart';

@JsonSerializable()
class ChatResponse {
  final String name;
  final String lastMessage;
  final String image;
  final String time;
  final bool isActive;

  const ChatResponse(
    this.name,
    this.lastMessage,
    this.image,
    this.time,
    this.isActive,
  );

  factory ChatResponse.fromJson(Map<String, dynamic> json) {
    return _$ChatResponseFromJson(json);
  }
}
