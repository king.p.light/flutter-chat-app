// Package imports:

// Project imports:
import 'package:dio/dio.dart';
import 'package:flutter_chat_app/data/api/application_api/application_api.dart';
import 'package:flutter_chat_app/data/api/application_api/response/chat_response.dart';

mixin ApplicationGetApiMixin on BaseAppApi {
//   Future<List<NotificationResponse>> getNotifications({
//     required String accessToken,
//     required int start,
//     required int length,
//   }) async {
//     final json = await executeApi(
//       () => dio.get(
//         "/notifications",
//         queryParameters: {
//           "start": start,
//           "length": length,
//         },
//         options: Options(
//           headers: {
//             "authorization": "Bearer $accessToken",
//           },
//         ),
//       ),
//     ) as List<dynamic>;

//     return json.map((response) {
//       return NotificationResponse.fromJson(
//         response as Map<String, dynamic>,
//       );
//     }).toList();
//   }

  Future<ChatResponse> getChat() async {
    final json = await executeApi(
      () => dio.get(
        '/chat',
        options: Options(
          headers: {
            "Content-type": "application/json; charset=utf-8",
          },
          responseType: ResponseType.plain,
        ),
      ),
    );
    return ChatResponse.fromJson(json as Map<String, dynamic>);
  }
}
