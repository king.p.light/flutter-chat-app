// Package imports:
import 'package:dio/dio.dart';
import 'package:flutter_chat_app/data/api/api_mixin.dart';
import 'package:flutter_chat_app/data/api/application_api/application_get_api_mixin.dart';
import 'package:flutter_chat_app/data/api/application_api/application_post_api_mixin.dart';
import 'package:flutter_chat_app/util/binary_converter.dart';

// Project imports:

class AppApi = BaseAppApi with ApplicationGetApiMixin, ApplicationPostApiMixin;

class BaseAppApi with ApiMixin {
  final Dio dio;

  const BaseAppApi({
    required this.dio,
  });

  BinaryConverter get binaryConverter => const BinaryConverter();
}
