// Package imports:
import 'dart:typed_data';
import 'package:dio/dio.dart';

// Project imports:
import 'package:flutter_chat_app/constant/exception/http_exception.dart';
import 'package:flutter_chat_app/constant/exception/unknown_exception.dart';
import 'package:flutter_chat_app/data/api/application_api/response/error_response.dart';
import 'package:flutter_chat_app/data/api/application_api/response/exception_response.dart';

mixin ApiMixin {
  Future<dynamic> executeApi(
    Future<Response<dynamic>> Function() handler,
  ) async {
    try {
      final response = await handler();
      return _handleResponse(response);
    } on DioError catch (error) {
      return _handleResponse(error.response);
    }
  }

  dynamic _handleResponse(Response<dynamic>? response) {
    final map = response?.data;

    if (map is Map<String, dynamic>) {
      if (map["status"] == "error") {
        late Exception exception;
        try {
          exception = ExceptionResponse.fromJson(map).toException();
        } catch (error) {
          // ignore: avoid_print
          print(error);

          throw UnknownException();
        }
        throw exception;
      }

      return map;
    } else if (map is Uint8List) {
      return map;
    }

    throw HttpException();
  }

  /// Execute asynchronous API call and extract data["data"] if exists
  Future<dynamic> extractNested(
    Future<Response<dynamic>> Function() handler,
  ) async {
    final data = await extract(handler);

    if (data["error"] != null) {
      throw ErrorResponse.fromJson(
        data["error"] as Map<String, dynamic>,
      ).asException();
    } else if (data["data"] != null) {
      return data["data"];
    } else {
      throw HttpException();
    }
  }

  /// Execute asynchronous API call and extract data if exists
  Future<dynamic> extract(
    Future<Response<dynamic>> Function() handler,
  ) async {
    final Response<dynamic> response;

    try {
      response = await handler();
    } on DioError catch (_) {
      throw HttpException();
    }

    if (response.data == null) throw HttpException();

    return response.data;
  }

  Future<dynamic> executeApiNullableData(
    Future<Response<dynamic>> Function() handler,
  ) async {
    try {
      final response = await handler();
      if (response.data == null) {
        return null;
      }

      return _handleResponse(response);
    } on DioError catch (error) {
      return _handleResponse(error.response);
    }
  }
}
