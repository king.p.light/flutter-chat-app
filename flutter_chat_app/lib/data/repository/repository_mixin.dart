// Project imports:
import 'package:flutter_chat_app/data/api/application_api/application_api.dart';
import 'package:flutter_chat_app/data/storage/secure_storage.dart';
import 'package:flutter_chat_app/data/storage/shared_prefs.dart';
import 'package:flutter_chat_app/util/locator.dart';

mixin RepositoryMixin {
  // final authApi = locator.get<AuthApi>();
  final applicationApi = locator.get<AppApi>();

  final secureStorage = locator.get<SecureStorage>();
  final sharedPrefs = locator.get<SharedPrefs>();

  // Future<String> getUsername() async {
  //   final username = await secureStorage.getUsername();

  //   if (username == null) throw AuthException.accessDenied;

  //   return username;
  // }

  // Future<String> getAccessToken() async {
  //   final accessToken = await secureStorage.getAccessToken();
  //   // final expirationDate = await sharedPrefs.getAccessTokenExpirationDate();

  //   // if (accessToken == null ||
  //   // expirationDate == null ||
  //   // DateTime.now().isAfter(expirationDate))
  //   if (accessToken == null) throw AuthException.accessDenied;

  //   return accessToken;
  // }

  // Future<String> getRefreshToken() async {
  //   final refreshToken = await secureStorage.getRefreshToken();
  //   final expirationDate = await sharedPrefs.getAccessTokenExpirationDate();

  //   if (refreshToken == null ||
  //       expirationDate == null ||
  //       DateTime.now().isAfter(expirationDate))
  //     throw AuthException.accessDenied;

  //   return refreshToken;
  // }
}
