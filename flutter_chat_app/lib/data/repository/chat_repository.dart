import 'package:flutter_chat_app/data/repository/repository_mixin.dart';
import 'package:flutter_chat_app/model/chat.dart';

class ChatRepository with RepositoryMixin {
  Future<Map<String, Chat>> fetchChat() async {
    final res = await applicationApi.getChat();
    final resChat = Chat.fromChatResponse(res);

    return {
      "data": resChat,
    };
  }
}
