// Dart imports:
import 'dart:convert';
import 'dart:typed_data';

// Package imports:
import 'package:json_annotation/json_annotation.dart';

class BinaryConverter implements JsonConverter<Uint8List, String> {
  const BinaryConverter();

  @override
  Uint8List fromJson(String json) => base64Decode(json);

  @override
  String toJson(Uint8List object) => base64Encode(object);
}
