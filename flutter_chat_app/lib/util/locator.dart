// Flutter imports:

// Package imports:
// import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'package:get_it/get_it.dart';
import 'package:flutter_chat_app/constant/enum/env.dart';
// import 'package:shared_preferences/shared_preferences.dart';

// Project imports:

final locator = GetIt.instance;

void setupLocator() {
  // Environment variable
  locator.registerLazySingleton<Env>(() => Env.stg);

  // // OAuth Client
  // locator.registerLazySingleton(
  //   () => locator.get<Env>().oauthClient,
  // );

  // // Auth Api Endpoint
  // locator.registerLazySingleton(
  //   () => AuthApi(dio: locator.get<Env>().authApiEndpoint),
  // );

  // locator.registerLazySingleton(
  //   () => AppApi(dio: locator.get<Env>().applicationApiEndpoint),
  // );

  // // Storage
  // locator.registerLazySingleton(() {
  //   return kIsWeb ? MockFlutterSecureStorage() : const FlutterSecureStorage();
  // });
  // locator.registerLazySingletonAsync(
  //   () async => await SharedPreferences.getInstance(),
  // );
  // locator.registerLazySingleton(() => SecureStorage());
  // locator.registerLazySingleton(() => SharedPrefs());

  // Repository
  // locator.registerLazySingleton(() => AuthRepository());
}
