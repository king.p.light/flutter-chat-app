// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:easy_localization/easy_localization.dart';

// Project imports:
import 'package:flutter_chat_app/constant/color_constant.dart';

mixin SearchHeaderProviderMixin<T extends StatefulWidget> on State<T> {
  final searchTextController = TextEditingController();
  final searchFocusNode = FocusNode();

  @override
  void dispose() {
    searchTextController.dispose();
    searchFocusNode.dispose();

    super.dispose();
  }

  void Function()? showFilterDialog() => null;
  Future<void>? onEditingComplete() => null;

  Widget buildSearchHeader() {
    final showFilterDialog = this.showFilterDialog();

    return SizedBox(
      height: 100,
      child: Padding(
        padding: const EdgeInsets.all(18),
        child: Row(
          children: [
            Expanded(
              child: buildSearchTextField(),
            ),
            if (showFilterDialog != null) ...[
              const SizedBox(width: 18),
              _buildFilterButton(showFilterDialog),
            ]
          ],
        ),
      ),
    );
  }

  Widget buildSearchTextField() {
    return Theme(
      data: Theme.of(context).copyWith(
        inputDecorationTheme: InputDecorationTheme(
          contentPadding: const EdgeInsets.symmetric(vertical: 10),
          border: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30.0),
          ),
          enabledBorder: OutlineInputBorder(
            borderRadius: BorderRadius.circular(30.0),
            borderSide: const BorderSide(color: ColorConstant.darkGray),
          ),
        ),
      ),
      child: TextFormField(
        controller: searchTextController,
        focusNode: searchFocusNode,
        onEditingComplete: onEditingComplete,
        decoration: InputDecoration(
          hintText: "search_header_provider_mixin.search_textfield".tr(),
          prefixIcon: const Icon(
            Icons.search,
            color: ColorConstant.darkGray,
          ),
          prefixIconConstraints: const BoxConstraints(minWidth: 60),
        ),
      ),
    );
  }

  Widget _buildFilterButton(
    void Function() showFilterDialog,
  ) {
    return InkWell(
      onTap: showFilterDialog,
      child: Container(
        padding: const EdgeInsets.all(12),
        decoration: BoxDecoration(shape: BoxShape.circle, border: Border.all()),
        child: const Icon(
          Icons.filter_list,
          color: ColorConstant.darkGray,
          size: 25,
        ),
      ),
    );
  }
}
