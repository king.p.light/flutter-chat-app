// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:easy_localization/easy_localization.dart';

mixin ConfirmCloseProviderMixin<T extends StatefulWidget> on State<T> {
  bool isEdited = false;

  Widget buildWillPopScope({
    required Widget child,
  }) {
    return WillPopScope(
      onWillPop: () async {
        if (isEdited) return await _showDialog();

        return true;
      },
      child: child,
    );
  }

  Widget _buildDialog(BuildContext context) {
    return AlertDialog(
      title: Row(
        children: [
          const Icon(
            Icons.warning,
            color: Colors.red,
            size: 48,
          ),
          const Text("confirm_close_provider_mixin.title").tr(),
        ],
      ),
      content: const Text("confirm_close_provider_mixin.title").tr(),
      actions: [
        OutlinedButton(
          onPressed: () => Navigator.of(context).pop(false),
          child: const Text("confirm_close_provider_mixin.cancel_button").tr(),
        ),
        ElevatedButton(
          onPressed: () => Navigator.of(context).pop(true),
          child: const Text("confirm_close_provider_mixin.close_button").tr(),
        ),
      ],
    );
  }

  Future<bool> _showDialog() async {
    final result = await showDialog<bool>(
      context: context,
      useRootNavigator: true,
      builder: (context) => _buildDialog(context),
    );

    return result ?? false;
  }
}
