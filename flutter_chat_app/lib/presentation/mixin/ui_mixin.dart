// Flutter imports:
import 'package:flutter/material.dart';
import 'package:flutter/scheduler.dart';

// Package imports:
import 'package:easy_localization/easy_localization.dart';
import 'package:flutter_chat_app/constant/exception/auth_exception.dart';
import 'package:flutter_chat_app/constant/exception/http_exception.dart';
import 'package:flutter_chat_app/constant/exception/unknown_exception.dart';
import 'package:flutter_chat_app/presentation/widget/loading_dialog.dart';

// Project imports:

mixin UiMixin<T extends StatefulWidget> on State<T> {
  BuildContext? _dialogContext;

  Future<void> executeUi(
    Future<void> Function() handler, {
    Future<void> Function()? successHandler,
    String? confirmText,
    String? loadingText,
    String? successText,
  }) async {
    await SchedulerBinding.instance.endOfFrame;

    bool dismissed = false;

    if (confirmText != null) {
      final confirmed = await showConfirmDialog(title: confirmText);

      if (!confirmed) return;
    }

    try {
      if (loadingText != null)
        // ignore: curly_braces_in_flow_control_structures
        showDialog(
          context: context,
          barrierDismissible: false,
          useRootNavigator: true,
          builder: (context) {
            _dialogContext = context;

            if (dismissed) _dismissDialog();

            return LoadingDialog(text: loadingText);
          },
        );

      await handler();

      dismissed = true;

      _dismissDialog();

      if (successText != null) showSnackBar(successText);
      if (successHandler != null) await successHandler();
    } on AuthException catch (exception) {
      dismissed = true;

      _dismissDialog();

      if (exception == AuthException.accessDenied) {
        await showAlertDialog(
          title: exception.title,
          content: exception.content,
        );

        if (!mounted) return;

        // await context.read<AuthCubit>().logout();
      } else {
        rethrow;
      }
    } on HttpException {
      dismissed = true;

      _dismissDialog();

      await showNetworkErrorAlertDialog();
    } on UnknownException {
      dismissed = true;

      _dismissDialog();

      await showUnknownErrorAlertDialog();
    } on Exception {
      dismissed = true;

      _dismissDialog();

      rethrow;
    }
  }

  Future<U?> showAlertDialog<U>({
    String? title,
    String? content,
  }) async {
    return await showDialog<U>(
      context: context,
      useRootNavigator: false,
      builder: (context) {
        return AlertDialog(
          title: title != null ? Text(title) : null,
          content: content != null ? Text(content) : null,
          actions: [
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: const Text("ui_mixin.alert_dialog.positive_button").tr(),
            ),
          ],
        );
      },
    );
  }

  Future<bool> showConfirmDialog({
    String? title,
    String? content,
  }) async {
    return await showDialog<bool>(
          context: context,
          builder: (context) {
            return AlertDialog(
              title: Text(title ?? "account_page.confirm_dialog.title".tr()),
              content: content != null ? Text(content) : null,
              actions: [
                TextButton(
                  onPressed: () => Navigator.of(context).pop(false),
                  child: const Text(
                          "account_page.confirm_dialog.cancel_log_out_button")
                      .tr(),
                ),
                TextButton(
                  onPressed: () => Navigator.of(context).pop(true),
                  child: const Text(
                          "account_page.confirm_dialog.confirm_log_out_button")
                      .tr(),
                ),
              ],
            );
          },
        ) ??
        false;
  }

  Future<void> showNetworkErrorAlertDialog() async {
    await showAlertDialog(
      title: "ui_mixin.network_error_dialog.title".tr(),
      content: "ui_mixin.network_error_dialog.content".tr(),
    );
  }

  Future<void> showUnknownErrorAlertDialog() async {
    await showAlertDialog(
      title: "ui_mixin.unknown_error_dialog.title".tr(),
      content: "ui_mixin.unknown_error_dialog.content".tr(),
    );
  }

  void _dismissDialog() {
    final dialogContext = _dialogContext;

    if (dialogContext != null) Navigator.of(dialogContext).pop();

    _dialogContext = null;
  }

  void showSnackBar(String content) {
    final scaffoldMessenger = ScaffoldMessenger.of(context);

    scaffoldMessenger.clearSnackBars();
    scaffoldMessenger.showSnackBar(
      SnackBar(
        content: Text(content),
      ),
    );
  }
}
