// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:
import 'package:easy_localization/easy_localization.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

mixin PullToRefreshProviderMixin<T extends StatefulWidget> on State<T> {
  final refreshController = RefreshController(initialRefresh: true);

  Widget buildRefresher({
    required Widget child,
  }) {
    return SmartRefresher(
      controller: refreshController,
      onRefresh: onScrollToTop(),
      onLoading: onScrollToBottom(),
      enablePullUp: onScrollToBottom() != null,
      footer: const ClassicFooter(
        loadStyle: LoadStyle.ShowWhenLoading,
      ),
      child: child,
    );
  }

  Widget buildEmptyList() {
    return ListView(
      children: [buildEmptyContent()],
    );
  }

  Widget buildLoadingList() {
    return ListView(
      children: [buildLoadingContent()],
    );
  }

  Widget buildEmptyContent() {
    return ListTile(
      title: const Text(
        "pull_to_refresh_provider_mixin.empty_list_text",
      ).tr(),
    );
  }

  Widget buildLoadingContent() {
    return ListTile(
      title: const Text(
        "pull_to_refresh_provider_mixin.loading_list_text",
      ).tr(),
    );
  }

  void Function()? onScrollToTop() => null;

  void Function()? onScrollToBottom() => null;
}
