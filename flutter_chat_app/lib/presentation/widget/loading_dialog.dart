// Flutter imports:
import 'package:flutter/material.dart';

class LoadingDialog extends StatelessWidget {
  final String text;

  const LoadingDialog({Key? key, required this.text}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      title: Text(text),
      content: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          SizedBox(
            width: 60,
            height: 60,
            child: CircularProgressIndicator(),
          ),
        ],
      ),
    );
  }
}
