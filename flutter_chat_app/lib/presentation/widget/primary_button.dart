import 'package:flutter/src/foundation/key.dart';
import 'package:flutter/src/widgets/framework.dart';

// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:

// Project imports:
import 'package:flutter_chat_app/constant/color_constant.dart';

class PrimaryButton extends StatelessWidget {
  final String text;
  final VoidCallback onPressed;
  final Color color;
  final EdgeInsets padding;

  const PrimaryButton({
    Key? key,
    required this.text,
    required this.onPressed,
    required this.color,
    required this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      shape: const RoundedRectangleBorder(
        borderRadius: BorderRadius.all(Radius.circular(40.0)),
      ),
      padding: padding,
      color: color,
      minWidth: double.infinity,
      child: Text(
        text,
        style: const TextStyle(
          color: ColorConstant.white,
        ),
      ),
    );
  }
}
