// Flutter imports:
import 'package:flutter/material.dart';

// Package imports:

// Project imports:
import 'package:flutter_chat_app/constant/color_constant.dart';

class FilledOutlineButton extends StatelessWidget {
  final bool isFilled;
  final VoidCallback onPressed;
  final String text;

  const FilledOutlineButton({
    Key? key,
    required this.isFilled,
    required this.onPressed,
    required this.text,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MaterialButton(
      onPressed: onPressed,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(30.0),
        side: const BorderSide(color: ColorConstant.white),
      ),
      color: isFilled ? ColorConstant.white : Colors.transparent,
      child: Text(
        text,
        style: TextStyle(
          color:
              isFilled ? ColorConstant.kContentColorLightTheme : Colors.white,
          fontSize: 12.0,
        ),
      ),
    );
  }
}
