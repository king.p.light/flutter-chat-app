// Package imports:
import 'package:easy_localization/easy_localization.dart';

class UnknownException implements Exception {
  final String title;
  final String content;

  UnknownException({
    String? title,
    String? content,
  })  : title = title ?? "ui_mixin.unknown_error_dialog.title".tr(),
        content = content ?? "ui_mixin.unknown_error_dialog.content".tr();
}
