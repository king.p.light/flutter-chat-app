// Package imports:
import 'package:easy_localization/easy_localization.dart';

class PermissionException implements Exception {
  final String title;
  final String content;

  const PermissionException._({
    required this.title,
    required this.content,
  });

  static final camera = PermissionException._(
    title: "permission_exception.camera.title".tr(),
    content: "permission_exception.camera.content".tr(),
  );

  static final location = PermissionException._(
    title: "permission_exception.location.title".tr(),
    content: "permission_exception.location.content".tr(),
  );

  static final storage = PermissionException._(
    title: "permission_exception.storage.title".tr(),
    content: "permission_exception.storage.content".tr(),
  );
}
