// Package imports:
import 'package:easy_localization/easy_localization.dart';

class JwtException implements Exception {
  final String content;

  const JwtException._({
    required this.content,
  });

  static final illegalBase64String = JwtException._(
    content: "jwt_exception.illegal_base64_string.content".tr(),
  );

  static final invalidPayload = JwtException._(
    content: "jwt_exception.invalid_payload.content".tr(),
  );

  static final invalidToken = JwtException._(
    content: "jwt_exception.invalid_token.content".tr(),
  );
}
