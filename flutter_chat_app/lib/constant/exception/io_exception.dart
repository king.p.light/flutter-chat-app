// Package imports:
import 'package:easy_localization/easy_localization.dart';

class IoException implements Exception {
  final String content;

  const IoException._({
    required this.content,
  });

  static final downloadFailed = IoException._(
    content: "io_exception.download_failed.content".tr(),
  );

  static final generateQrCodeFailed = IoException._(
    content: "io_exception.generate_qr_code_failed.content".tr(),
  );

  static final savePhotoFailed = IoException._(
    content: "io_exception.save_photo_failed.content".tr(),
  );

  static final readQrFailed = IoException._(
    content: "io_exception.read_qr_failed.content".tr(),
  );
}
