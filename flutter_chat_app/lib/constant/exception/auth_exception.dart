// Package imports:
import 'package:easy_localization/easy_localization.dart';

class AuthException implements Exception {
  final String title;
  final String content;

  const AuthException._({
    required this.title,
    required this.content,
  });

  static final accessDenied = AuthException._(
    title: "auth_exception.access_denied.title".tr(),
    content: "auth_exception.access_denied.content".tr(),
  );

  static final invalidGrant = AuthException._(
    title: "auth_exception.invalid_grant.title".tr(),
    content: "auth_exception.invalid_grant.content".tr(),
  );

  static final notApplicable = AuthException._(
    title: "auth_exception.not_applicable.title".tr(),
    content: "auth_exception.not_applicable.content".tr(),
  );

  static final notRegistered = AuthException._(
    title: "auth_exception.not_registered.title".tr(),
    content: "auth_exception.not_registered.content".tr(),
  );

  static final userCancelled = AuthException._(
    title: "auth_exception.user_cancelled.title".tr(),
    content: "auth_exception.user_cancelled.content".tr(),
  );

  static final usernameAlreadyTaken = AuthException._(
    title: "auth_exception.username_already_taken.title".tr(),
    content: "auth_exception.username_already_taken.content".tr(),
  );
}
