// Package imports:
import 'package:easy_localization/easy_localization.dart';

class APIException implements Exception {
  final String title;
  final String content;

  const APIException._({
    required this.title,
    required this.content,
  });

  static final stationNotFound = APIException._(
    title: "application_exception.station_not_found.title".tr(),
    content: "application_exception.station_not_found.content".tr(),
  );

  static final fileNotFound = APIException._(
    title: "application_exception.file_not_found.title".tr(),
    content: "application_exception.file_not_found.content".tr(),
  );

  static final notFound = APIException._(
    title: "application_exception.not_found.title".tr(),
    content: "application_exception.not_found.content".tr(),
  );
}
