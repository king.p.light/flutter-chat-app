class ValidationException implements Exception {
  final List<String> messages;

  const ValidationException(this.messages);
}
