// Flutter imports:
import 'package:flutter/material.dart';

class ColorConstant {
  const ColorConstant._();

  static const kPrimaryColor = Color.fromRGBO(0, 191, 109, 1);
  static const kSecondaryColor = Color.fromRGBO(254, 153, 1, 1);
  static const kContentColorLightTheme = Color.fromRGBO(29, 29, 53, 1);
  static const kContentColorDarkTheme = Color.fromRGBO(245, 252, 249, 1);
  static const kWarninngColor = Color.fromRGBO(243, 187, 28, 1);
  static const kErrorColor = Color.fromRGBO(240, 55, 56, 1);

  static const kDefaultPadding = 20.0;

  static const deepBlue = Color.fromRGBO(3, 73, 162, 1);
  static const blue = Color.fromRGBO(27, 127, 188, 1);
  static const accentBlue = Color.fromRGBO(16, 119, 211, 1);

  static const deepGreen = Color.fromRGBO(0, 148, 68, 1);
  static const aquaGreen = Color.fromRGBO(76, 167, 157, 1);
  static const green = Color.fromRGBO(57, 181, 74, 1);
  static const leafGreen = Color.fromRGBO(141, 198, 63, 1);

  static const deepYellow = Color.fromRGBO(255, 194, 14, 1);
  static const yellow = Color.fromRGBO(255, 212, 0, 1);

  static const white = Color.fromRGBO(255, 255, 255, 1);
  static const lightGray = Color.fromRGBO(224, 224, 224, 1);
  static const gray = Color.fromRGBO(0, 0, 0, 0.4);
  static const darkGray = Color.fromRGBO(62, 62, 62, 1);
  static const black = Color.fromRGBO(0, 0, 0, 1);

  static const error = Color.fromRGBO(176, 0, 32, 1);

  static const inputHint = Color.fromRGBO(0, 0, 0, 0.6);
  static const inputLabel = Color.fromRGBO(0, 0, 0, 0.6);
  static const inputBorder = Color.fromRGBO(0, 0, 0, 0.12);

  static const disabled = Colors.black38;

  static const highEmphasis = Color.fromRGBO(0, 0, 0, 0.87);

  static const hover = Color.fromRGBO(0, 0, 0, 0.04);

  static const primarySwatch = MaterialColor(
    0xFF0348A2,
    {
      50: Color.fromRGBO(227, 242, 253, 1),
      100: Color.fromRGBO(187, 222, 251, 1),
      200: Color.fromRGBO(143, 203, 249, 1),
      300: Color.fromRGBO(99, 182, 246, 1),
      400: Color.fromRGBO(63, 166, 246, 1),
      500: Color.fromRGBO(26, 151, 244, 1),
      600: Color.fromRGBO(22, 137, 230, 1),
      700: Color.fromRGBO(16, 119, 211, 1),
      800: Color.fromRGBO(12, 102, 193, 1),
      900: Color.fromRGBO(3, 72, 162, 1),
    },
  );

  static const secondarySwatch = MaterialColor(
    0xFF8EC63F,
    {
      50: Color.fromRGBO(242, 248, 232, 1),
      100: Color.fromRGBO(221, 238, 198, 1),
      200: Color.fromRGBO(199, 227, 161, 1),
      300: Color.fromRGBO(177, 215, 123, 1),
      400: Color.fromRGBO(159, 207, 93, 1),
      500: Color.fromRGBO(142, 198, 63, 1),
      600: Color.fromRGBO(126, 182, 55, 1),
      700: Color.fromRGBO(105, 162, 45, 1),
      800: Color.fromRGBO(85, 142, 36, 1),
      900: Color.fromRGBO(49, 108, 17, 1),
    },
  );
}
