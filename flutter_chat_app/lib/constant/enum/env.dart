// Package imports:

// Project imports:

/// Build Environment
enum Env {
  /// Mock Environment
  mock,

  /// Staging Environment
  local,

  /// Staging Environment
  stg,

  /// Production Environment
  prod,
}

extension EnvExtension on Env {
  // /// OAuth Client
  // OAuthClient get oauthClient {
  //   switch (this) {
  //     case Env.mock:
  //       return MockOAuthClient();

  //     case Env.local:
  //       return MockOAuthClient();

  //     case Env.stg:
  //       return MockOAuthClient();
  //     case Env.prod:
  //       return MockOAuthClient();
  //   }
  // }

  // /// Auth Api Endpoint
  // Dio get authApiEndpoint {
  //   switch (this) {
  //     case Env.mock:
  //       return MockAuthEndpoint();

  //     case Env.local:
  //       return Dio(
  //         BaseOptions(baseUrl: "http://localhost:4000/api"),
  //       );

  //     case Env.stg:
  //       return MockAuthEndpoint();
  //     case Env.prod:
  //       return MockAuthEndpoint();
  //   }
  // }

  // Dio get applicationApiEndpoint {
  //   switch (this) {
  //     case Env.mock:
  //       return MockAPIEndpoint();

  //     case Env.local:
  //       return Dio(
  //         BaseOptions(baseUrl: "http://localhost:4000/api"),
  //       );

  //     case Env.stg:
  //       return Dio(
  //         BaseOptions(
  //           baseUrl:
  //               "https://elastic.snaplogic.com/api/1/rest/slsched/feed/JumpAI/Siriraj/QR_System",
  //           headers: {
  //             "Authorization": "Bearer W1Lg5fnmZZDdgF79cz6gp3sLrvveBjq3",
  //           },
  //         ),
  //       );

  //     case Env.prod:
  //       return MockAPIEndpoint();
  //   }
  // }
}
