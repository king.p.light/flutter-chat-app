// Flutter imports:
import 'package:flutter/material.dart';

// Project imports:
import 'package:flutter_chat_app/constant/color_constant.dart';

import 'package:flutter_chat_app/constant/color_constant.dart';
import 'package:google_fonts/google_fonts.dart';

class ThemeConstant {
  const ThemeConstant._();

  static final theme = ThemeData(
    fontFamily: "DB Heavent",
    primarySwatch: Colors.green,
    scaffoldBackgroundColor: ColorConstant.white,
    appBarTheme: appBarTheme,
    inputDecorationTheme: inputDecorationTheme,
    elevatedButtonTheme: elevatedButtonTheme(),
    outlinedButtonTheme: outlinedButtonTheme(),
    cardTheme: cardTheme,
    iconTheme: iconTheme,
    checkboxTheme: checkboxTheme,
    tabBarTheme: tabBarTheme,
    bottomNavigationBarTheme: bottomNavigationBarTheme,
    textTheme: textTheme,
  );

  static const appBarTheme = AppBarTheme(
    titleTextStyle: TextStyle(
      fontFamily: "Roboto",
      fontSize: 20,
      fontWeight: FontWeight.w500,
      height: 24 / 20,
      letterSpacing: 0.15,
    ),
  );

  static final inputDecorationTheme = InputDecorationTheme(
    contentPadding: const EdgeInsets.symmetric(horizontal: 16),
    hintStyle: const TextStyle(color: ColorConstant.inputHint, height: 0.75),
    labelStyle:
        TextStyle(color: ColorConstant.primarySwatch.shade700, height: 0.75),
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(8),
      borderSide: const BorderSide(
        color: ColorConstant.accentBlue,
      ),
    ),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(8),
      borderSide: const BorderSide(
        color: ColorConstant.inputBorder,
      ),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(8),
      borderSide: const BorderSide(
        color: ColorConstant.accentBlue,
      ),
    ),
  );

  static final underlinedInputDecorationTheme = InputDecorationTheme(
    contentPadding: const EdgeInsets.symmetric(horizontal: 16, vertical: 12),
    hintStyle: const TextStyle(color: ColorConstant.inputHint, height: 0.75),
    labelStyle:
        TextStyle(color: ColorConstant.primarySwatch.shade700, height: 0.75),
    border: const UnderlineInputBorder(
      borderSide: BorderSide(
        color: ColorConstant.darkGray,
      ),
    ),
    enabledBorder: const UnderlineInputBorder(
      borderSide: BorderSide(
        color: ColorConstant.darkGray,
      ),
    ),
    focusedBorder: const UnderlineInputBorder(
      borderSide: BorderSide(
        color: ColorConstant.darkGray,
      ),
    ),
  );

  static final roundedInputDecorationTheme = InputDecorationTheme(
    contentPadding: const EdgeInsets.symmetric(horizontal: 16),
    hintStyle: const TextStyle(color: ColorConstant.darkGray, height: 0.75),
    labelStyle: const TextStyle(color: ColorConstant.darkGray, height: 0.75),
    border: OutlineInputBorder(
      borderRadius: BorderRadius.circular(100),
      borderSide: const BorderSide(
        color: ColorConstant.accentBlue,
      ),
    ),
    enabledBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(100),
    ),
    focusedBorder: OutlineInputBorder(
      borderRadius: BorderRadius.circular(100),
      borderSide: const BorderSide(
        color: ColorConstant.accentBlue,
      ),
    ),
  );

  static ElevatedButtonThemeData elevatedButtonTheme({
    Color color = Colors.white,
    Color backgroundColor = ColorConstant.deepBlue,
  }) {
    return ElevatedButtonThemeData(
      style: ButtonStyle(
        padding: MaterialStateProperty.all(
          const EdgeInsets.symmetric(horizontal: 10, vertical: 6),
        ),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
        ),
        foregroundColor: MaterialStateProperty.resolveWith((states) {
          return states.contains(MaterialState.disabled)
              ? Colors.black38
              : color;
        }),
        backgroundColor: MaterialStateProperty.resolveWith((states) {
          return states.contains(MaterialState.disabled)
              ? Colors.grey
              : backgroundColor;
        }),
        elevation: MaterialStateProperty.all(0),
      ),
    );
  }

  static OutlinedButtonThemeData outlinedButtonTheme({
    Color color = ColorConstant.accentBlue,
  }) {
    return OutlinedButtonThemeData(
      style: ButtonStyle(
        padding: MaterialStateProperty.all(
          const EdgeInsets.symmetric(horizontal: 12, vertical: 6),
        ),
        shape: MaterialStateProperty.all(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(100),
          ),
        ),
        side: MaterialStateProperty.resolveWith((states) {
          return BorderSide(
            color: states.contains(MaterialState.disabled)
                ? Colors.black38
                : color,
          );
        }),
        foregroundColor: MaterialStateProperty.resolveWith((states) {
          return states.contains(MaterialState.disabled)
              ? Colors.black38
              : color;
        }),
      ),
    );
  }

  static const cardTheme = CardTheme(
    margin: EdgeInsets.symmetric(horizontal: 12, vertical: 6),
  );

  static const iconTheme = IconThemeData(
    color: ColorConstant.aquaGreen,
  );

  static final checkboxTheme = CheckboxThemeData(
    fillColor: MaterialStateProperty.all(ColorConstant.primarySwatch.shade700),
    side: BorderSide(
      color: ColorConstant.primarySwatch.shade700,
      width: 2,
    ),
  );

  static const tabBarTheme = TabBarTheme(
    indicator: ShapeDecoration(
      shape: UnderlineInputBorder(
        borderSide: BorderSide(
          color: Colors.white,
          width: 2,
        ),
      ),
    ),
  );

  static const bottomNavigationBarTheme = BottomNavigationBarThemeData(
    backgroundColor: ColorConstant.blue,
    unselectedItemColor: Color.fromRGBO(255, 255, 255, 0.74),
    selectedItemColor: Colors.white,
    unselectedLabelStyle: TextStyle(
      fontFamily: "Roboto",
      fontWeight: FontWeight.w400,
      fontSize: 12,
      height: 16 / 12,
      letterSpacing: 0.4,
    ),
    selectedLabelStyle: TextStyle(
      fontFamily: "Roboto",
      fontWeight: FontWeight.w400,
      fontSize: 12,
      height: 16 / 12,
      letterSpacing: 0.4,
    ),
  );

  static const textTheme = TextTheme(
    headline1: TextStyle(
      color: Color.fromRGBO(0, 0, 0, 0.87),
      fontWeight: FontWeight.w300,
      fontSize: 144,
      height: 172.62 / 144,
      // how? letterSpacing: -3%,
    ),
    headline2: TextStyle(
      color: Color.fromRGBO(0, 0, 0, 0.87),
      fontWeight: FontWeight.w300,
      fontSize: 96,
      height: 72 / 96,
      letterSpacing: -4.5,
    ),
    headline3: TextStyle(
      color: Color.fromRGBO(0, 0, 0, 0.87),
      fontWeight: FontWeight.w400,
      fontSize: 72,
      height: 56 / 72,
    ),
    headline4: TextStyle(
      color: Color.fromRGBO(0, 0, 0, 0.87),
      fontWeight: FontWeight.w400,
      fontSize: 48,
      height: 36 / 48,
    ),
    headline5: TextStyle(
      color: Color.fromRGBO(0, 0, 0, 0.87),
      fontWeight: FontWeight.w700,
      fontSize: 36,
      height: 24 / 36,
      letterSpacing: 0.18,
    ),
    headline6: TextStyle(
      color: Color.fromRGBO(0, 0, 0, 0.87),
      fontWeight: FontWeight.w500,
      fontSize: 30,
      height: 24 / 30,
      letterSpacing: 0.15,
    ),
    subtitle1: TextStyle(
      color: Color.fromRGBO(0, 0, 0, 0.87),
      fontWeight: FontWeight.w500,
      fontSize: 24,
      height: 24 / 24,
      letterSpacing: 0.15,
    ),
    subtitle2: TextStyle(
      color: Color.fromRGBO(0, 0, 0, 0.87),
      fontWeight: FontWeight.w500,
      fontSize: 20,
      height: 24 / 20,
      letterSpacing: 0.1,
    ),
    bodyText1: TextStyle(
      color: Color.fromRGBO(0, 0, 0, 0.87),
      fontWeight: FontWeight.w400,
      fontSize: 24,
      height: 24 / 24,
      letterSpacing: 0.5,
    ),
    bodyText2: TextStyle(
      color: Color.fromRGBO(0, 0, 0, 0.87),
      fontWeight: FontWeight.w400,
      fontSize: 20,
      height: 20 / 20,
      letterSpacing: 0.25,
    ),
    button: TextStyle(
      color: Color.fromRGBO(0, 0, 0, 0.87),
      fontWeight: FontWeight.w500,
      fontSize: 20,
      letterSpacing: 0.5,
    ),
    caption: TextStyle(
      color: Color.fromRGBO(0, 0, 0, 0.87),
      fontWeight: FontWeight.w400,
      fontSize: 18,
      letterSpacing: 0.4,
    ),
    overline: TextStyle(
      color: Color.fromRGBO(0, 0, 0, 0.87),
      fontWeight: FontWeight.w400,
      fontSize: 16,
      height: 16 / 16,
      letterSpacing: 0.15,
    ),
  );
}

ThemeData lightThemeData(BuildContext context) {
  return ThemeData.light().copyWith(
    primaryColor: ColorConstant.kPrimaryColor,
    scaffoldBackgroundColor: Colors.white,
    appBarTheme: appBarTheme,
    iconTheme: IconThemeData(color: ColorConstant.kContentColorLightTheme),
    textTheme: GoogleFonts.interTextTheme(Theme.of(context).textTheme)
        .apply(bodyColor: ColorConstant.kContentColorLightTheme),
    colorScheme: ColorScheme.light(
      primary: ColorConstant.kPrimaryColor,
      secondary: ColorConstant.kSecondaryColor,
      error: ColorConstant.kErrorColor,
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: Colors.white,
      selectedItemColor: ColorConstant.kContentColorLightTheme.withOpacity(0.7),
      unselectedItemColor:
          ColorConstant.kContentColorLightTheme.withOpacity(0.32),
      selectedIconTheme: IconThemeData(color: ColorConstant.kPrimaryColor),
      showUnselectedLabels: true,
    ),
  );
}

ThemeData darkThemeData(BuildContext context) {
  // Bydefault flutter provie us light and dark theme
  // we just modify it as our need
  return ThemeData.dark().copyWith(
    primaryColor: ColorConstant.kPrimaryColor,
    scaffoldBackgroundColor: ColorConstant.kContentColorLightTheme,
    appBarTheme: appBarTheme,
    iconTheme: IconThemeData(color: ColorConstant.kContentColorDarkTheme),
    textTheme: GoogleFonts.interTextTheme(Theme.of(context).textTheme)
        .apply(bodyColor: ColorConstant.kContentColorDarkTheme),
    colorScheme: ColorScheme.dark().copyWith(
      primary: ColorConstant.kPrimaryColor,
      secondary: ColorConstant.kSecondaryColor,
      error: ColorConstant.kErrorColor,
    ),
    bottomNavigationBarTheme: BottomNavigationBarThemeData(
      backgroundColor: ColorConstant.kContentColorLightTheme,
      selectedItemColor: Colors.white70,
      unselectedItemColor:
          ColorConstant.kContentColorDarkTheme.withOpacity(0.32),
      selectedIconTheme: IconThemeData(color: ColorConstant.kPrimaryColor),
      showUnselectedLabels: true,
    ),
  );
}

final appBarTheme = AppBarTheme(centerTitle: false, elevation: 0);
